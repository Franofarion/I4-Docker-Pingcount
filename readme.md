Docker - Ping Pong
===================
Par **DOMANGE** Baptiste et **VALENZA** Gauthier.

![Build Status](https://gitlab.com/Franofarion/I4-Docker-Pingcount/badges/master/build.svg)

Prérequis :
-------------
- NodeJS
- NPM
- JQ 

Certain script s'occuperont de faire un apt-get install, il est possible que le mot de passe sudo vous soit demandez.

Pour une meilleure stabilité des scripts et du serveur, il est quand même conseillé de les installer à la main. (nodejs & jq)

Lancement du serveur :
-------------
Lancer le fichier install_start_server.sh avec la commande :

    ./install_start_server.sh
    
Il est possible que nous n'aviez pas les droits, dans ce cas là :

    chmod 775 install_start_server.sh

Puis lancer le serveur avec le fichier.

Lors du lancement vous devrez entrer l'url DSN Sentry, si vous n'entrez pas d'URL nous utiliserons la notre.


Lancement des test unitaires :
-------------
Le serveur doit être lancé avant d'exécuter les tests unitaires.
Lancer le fichier unit_tests.sh avec la commande :

    ./unit_tests.sh
  
 Le script va tester si le serveur est bien lancé, s'il répond à un /ping et s'il renvoi la bonne valeur de pingCount.
   

Lancement des conteneurs Docker : 
-------------
Pour le serveur (faire npm install sur le serveur au préalable) : 
- `cd docker_Server`
- `docker build . --tag pingcount/server` pour le build
- `docker run --net="host" pingcount/server`

Pour les tests unitaires : 
- `cd docker_Unit_Test`
- `docker build . --tag pingcount/test` pour le build
- `docker run --net="host" -e "URL=http://localhost:3000/" pingcount/test` 

Récupérer les images Docker (par le Registery) :
-------------
First log in to GitLab’s Container Registry using your GitLab username and password. If you have 2FA enabled you need to use a personal access token:

    docker login registry.gitlab.com

### Pull

    docker pull registry.gitlab.com/franofarion/i4-docker-pingcount/pingcount/test:master
    docker pull registry.gitlab.com/franofarion/i4-docker-pingcount/pingcount/server:master

### Run (Automatically pull)

    docker run registry.gitlab.com/franofarion/i4-docker-pingcount/pingcount/test:master
    docker run registry.gitlab.com/franofarion/i4-docker-pingcount/pingcount/server:master