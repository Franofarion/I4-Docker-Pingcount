#!/bin/bash
#Check if jq (used to parse json) is installed on user's computer
sleep 10s

if ! [ -x "$(command -v jq)" ]; then
	printf "Installation de jq pour les tests unitaire"
	sudo apt get install jq
else
	printf "jq déjà installé sur votre poste"
fi

#Clear the console
clear
TESTPASSED=0
printf "Lancements des tests unitaires de PingCount : \\n\\n\\n" 

#TEST 1/2
printf "\\n\\nTest %sping : It should respond pong:\\n" "$URL"
PONG=$(curl -s "$URL"ping -w "\\n" | jq -r '.message')
echo "$PONG" 
if [ "$PONG" = "pong" ]; then
	printf "Test passed."
	TESTPASSED=$((TESTPASSED + 1))
else
	printf "Test not passed"
fi

#TEST 2/2
NUMBER=10
printf "\\n\\nTest %scount : It should add %s to current count\\n" "$URL" "$NUMBER"
BASE=$(curl -s "$URL"count -w "\\n" | jq -r '.pingCount') 
printf "Before test  : %s\\n" "$BASE"

for i in `seq 1 $NUMBER`;
do 
	curl "$URL"'ping' -w "\\n" > /dev/null 2>&1	
done

AFTER=$(curl -s "$URL"count -w "\\n" | jq -r '.pingCount') 
printf "After test  : %s\\n" "$AFTER"

if [ $((AFTER - NUMBER)) = "$BASE" ]; then
	printf "Test passed.\\n"
	TESTPASSED=$((TESTPASSED + 1))
else	
	printf "Test not passed\\n"
fi

printf "\\n\\n%s/2 tests passed \\n" "$TESTPASSED"
